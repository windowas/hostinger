<?php
session_start();
require_once('functions.php');
?>
<html>
    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    </head>

    <body>
        <div class="container">
            <form class="form" action="/index.php" method="POST">
                <label for="categories">Kur norėtumėte įterpti katogriją?</label>
                <select name="categories" class="form-control m-2">
                    <option value="0">Pagrindinė</option>

                    <?php

                    $theTree = new CategoriesTree();
                    $builtTree = unserialize($_SESSION['tree']['built']);

                    foreach ($theTree->showAllCategories($builtTree) as $category => $id) {
                        echo '<option value="' . $id . '">' . $category . '</option>';
                    }
                    ?>

                </select>

                <label for="name">Kategorijos pavadinimas</label>
                <input type="text" name="name" class="form-control m-1" required>

                <button type="submit" class="btn btn-success">Pridėti</button>
            </form>
            <a class="btn btn-warning" href="/index.php">Atgal</button></a>
        </div>
    </body>
</html>