<?php

class CategoriesTree {

    public $tree = [];
    public $categories = [];
    public $childs = [];
    public $countCategories = 0;
    public $depth = 0;

    public function insert($post) {
        if ($post['name']) {
            $id = sizeof($this->tree) + 1;
            $this->tree[] = (object)array('id' => $id, 'parent_id' => $post['categories'], 'name' => $post['name']);
        }
    }

    public function buildTree () {

        if ($this->tree) {
            $childs = [];

            foreach ($this->tree as $item) {
                $childs[$item->parent_id][] = $item;
            }

            foreach ($this->tree as $item) {
                if (isset($childs[$item->id])) {
                    $item->childs = $childs[$item->id];
                }
            }

            return $childs[0];
        }
    }

    public function setDepth($array) {
        if (isset($array)) {
            foreach ($array as &$item) {
                $item->depth = $this->depth;

                if (isset($item->childs)) {
                    $this->childs = array_merge($this->childs, $item->childs);
                }

                if (isset($children) && end($children) == $item) {
                    $children = [];
                }
            }

            if (isset($this->childs) && $this->childs) {
                $children = $this->childs;
                $this->childs = [];
                if (isset($children) && $children) {
                    $this->depth++;
                    $this->setDepth($children);
                }
            }
        }
    }

    public function displayTree ($tree) {


        if ($tree) {
            foreach ($tree as $item) {

                $prefix = str_repeat('&nbsp;&nbsp;', $item->depth) . '-';

                if (isset($item->childs)) {
                    echo $prefix . $item->name . "\n";
                } else {
                    echo $prefix . $item->name . "\n";
                }
                if (isset($item->childs)) {
                    $this->displayTree($item->childs);
                }
            }
        }
    }

    public function showAllCategories($tree) {
        if ($tree) {
            foreach ($tree as $item) {
                if (isset($item->name)) {
                    $this->categories[$item->name] = $item->id;
                    $this->countCategories++;
                }

                if (isset($item->childs)) {
                    $this->showAllCategories($item->childs);
                }
            }
            return $this->categories;
        }
    }
}
?>