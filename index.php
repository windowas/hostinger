<?php
session_start();
require_once('functions.php');
?>
<html>

    <head>
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    </head>

    <body>
        <div class="container">
            <h1>Kategorijų lentelė</h1>
            <?php

            $theTree = new CategoriesTree();

            if (isset($_SESSION['tree']['data'])) {
                $theTree = unserialize($_SESSION['tree']['data']);
            }

            if (isset($_POST) && $_POST) {
                $theTree->insert($_POST);
            }

            $builtTree = $theTree->buildTree();
            $theTree->depth = 0;
            $theTree->setDepth($builtTree);

            echo "<pre><ul class='list'>";
            $theTree->displayTree($builtTree);
            echo "</ul></pre>";

            $_SESSION['tree']['built'] = serialize($builtTree);
            $_SESSION['tree']['data'] = serialize($theTree);
            ?>

            <a href="/create.php" class="btn btn-primary">Sukurti kategoriją</a>
        </div>

    </body>
</html>


